/**
 * Application Settings
 */

var appSettings = {
    
    everlive: {
        apiKey: '23iu7FQuzlNKjrkE', // Put your Everlive API key here
        scheme: 'http'
    },
    
    eqatec: {
        productKey: '$EQATEC_PRODUCT_KEY$',  // Put your EQATEC product key here
        version: '1.0.0.0' // Put your application version here
    },
    
    facebook: {
        appId: '$FACEBOOK_APP_ID$', // Put your Facebook App ID here
        redirectUri: 'https://www.facebook.com/connect/login_success.html' // Put your Facebook Redirect URI here
    },
    
    google: {
        clientId: '$GOOGLE_CLIENT_ID$', // Put your Google Client ID here
        redirectUri: 'http://localhost' // Put your Google Redirect URI here
    },
    
    liveId: {
        clientId: '$LIVEID_CLIENT_ID$', // Put your LiveID Client ID here
        redirectUri: 'https://login.live.com/oauth20_desktop.srf' // Put your LiveID Redirect URI here
    },
    
    adfs: {
        adfsRealm: '$ADFS_REALM$', // Put your ADFS Realm here
        adfsEndpoint: '$ADFS_ENDPOINT$' // Put your ADFS Endpoint here
    },
    
    messages: {
        mistSimulatorAlert: 'The social login doesn\'t work in the In-Browser Client, you need to deploy the app to a device, or run it in the simulator of the Windows Client or Visual Studio.',
        removeActivityConfirm: 'Are you sure you want to delete this Activity?'
    }
};
