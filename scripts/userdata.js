

app.factory('userData', function() {

	var baseAddress = "http://www.d-stress.org:3000";

	var DataTypes = {
		boolean: 'boolean',
		object: 'object',
		lookup: 'lookup',
		number: 'number',
		collection: 'collection',
		int: 'int',
		float: 'float',
		array: 'array'
	};

    var ids = {};
    var serverAddress = baseAddress + "/api/data/";
    var timeOutDuration = 5000;
	var skipTests = 0;
	var maxSkipTests = 20 * 20;
    var timerId;
    var elements = {};



	function getKey(spec) {
        return spec.id + "_" + spec.type;
    }

    function DataElement(spec) {

        var self = this;
	    this.uniqueId = Radical.newGuid();
        this.type = spec.type;
        this.id = spec.id;
        this.structure = {};
        if(this.id) {
            if (!ids[this.id]) {
                addId(this.id);
            }
        }
        var stored = localStorage.getItem("stored" + getKey(spec));
        if(stored) {
            this.record = JSON.parse(stored);
        } else {
            this.record = {};
            this.record.data = spec.data || { _initialized: true };
        }
        this.lastUpdateTime = Date.now();
        this.parent = ids[this.id];
        elements[getKey(this)] = this;
        this.revision = 0;
        if(this.parent)
            this.parent.elements[this.type] = this;
        this.original = spec.data || {};
        Object.defineProperties(this, {
            fields: {
                get: function() { return this.record.data; }
            }
        });
        this.$updated = [];

        this.$written = [];
        this.$deferred = [];
        this.$once = [];
        this._ready = false;
        this.$readyList = [];
        this.$writeFunctions = [];

        this.write(function(defer) {
            if(defer) {
                return false;
            }
        }, function() {
            self._ready = true;
            Radical.invoke(self.$readyList, self);
            self.$readyList = [];
        });
        this.updated(function() {
            //localStorage.setItem("stored" + getKey(spec), JSON.stringify(self.record));
        });
    }
    DataElement.prototype.updateStructure = function() {
        verifyStructure(this.record.data, this.structure);
    };
    DataElement.prototype.isValid = function() {
        return (this.id && this.type);
    };
    DataElement.prototype.change = function(id, type) {
        var self = this;
        type = type || this.type;
        id = id || this.id;

        if(this.id == id && this.type == type) return this;
        if(this.parent) {
            delete this.parent.elements[this.type];
        }
        delete elements[getKey(this)];
        this.id = id;
        this.type = type;
        if(this.id) {
            if (!ids[this.id]) {
                addId(this.id);
            }
        }
        elements[getKey(this)] = this;
        this.parent = ids[this.id];
        if(this.parent) {
            this.parent.elements[this.type] = this;
        }
        var stored = localStorage.getItem("stored" + getKey(self));
	    stored = null;
        if(stored) {
            this.record = JSON.parse(stored);
        } else {
            this.record = {};
            this.record.data = { _initialized: true };
        }
        this.write(function(defer) {
            if(defer) {
                return false;
            }
        }, function() {
            self.ready = true;
            Radical.invoke(self.$readyList, self);
            self.$readyList = [];
        });
        return this;
    };
    DataElement.prototype.ready = function(fn) {
        if(this._ready)
            fn();
        else
            this.$readyList.push(fn);
    };
    DataElement.prototype.updated = function(fn) {
        this.$updated.push(fn);
        this.fireUpdates();
        return fn;
    };
    DataElement.prototype.deferred = function(fn) {
        this.$deferred.push(fn);
        return fn;
    };
    DataElement.prototype.written = function(fn) {
        this.$written.push(fn);
        return fn;
    };
    DataElement.prototype.once = function(fn) {
        this.$once.push(fn);
        return fn;
    };
    DataElement.prototype.field = function(f) {
        return this.record.data[f];
    };
    DataElement.prototype.update = function() {
        try {
	        Radical.invoke(this.$updated, this.record.data, this);
        } catch(e) {
	        Radical.invoke(updateList, this.id, this);
        }
    };
    DataElement.prototype.fireUpdates = function() {
        var self = this;
        if(self.updateTimer && (Date.now() - self.updateTime) < 1000)
            return;
        self.updateTime = Date.now();
        self.updateTimer = setTimeout(function() {
            delete self.updateTimer;
            self.update();
        },50);
    };

	function process(obj) {
		if(count(obj)==0) {
			obj.__initialized = true;
		} else {
			$.each(obj, function(i,v) {
				if(Radical.isObject(v) && !Radical.isArray(v)) {
					process(v);
				}
			});
		}
	}

	function count(obj) {

		if (obj.__count__ !== undefined) { // Old FF
			return obj.__count__;
		}

		if (Object.keys) { // ES5
			return Object.keys(obj).length;
		}

		// Everything else:

		var c = 0, p;
		for (p in obj) {
			if (obj.hasOwnProperty(p)) {
				c += 1;
			}
		}

		return c;

	}
	DataElement.prototype.define = function(definition) {
		if(!this.type) return this;

		for(var i in definition) {
			this.structure[i] = definition[i];
		}

		/* $.ajax({
		    url: baseAddress + "/api/structure/" + this.type,
		    type: 'post',
		    async: true,
		    data: {
			    structure: definition
		    }
		});*/
		return this;
	};

    DataElement.prototype.write = function(fn, onComplete, defer,time) {
        if(!this.isValid) return;
	    time = time || Date.now();
        var self = this;
	    if(self.writing) {
		    setTimeout(function() { self.write(fn, onComplete, defer, time); },10);
		    return;
	    }
	    defer = defer ? defer : false;
        if(onComplete)
            self.$once.push(onComplete);

        if(fn) {
            try {
                self.updateStructure();
                if (fn.call(self.record.data, defer, self) === false) {
                    if (self.$once.length > 0) {
                        Radical.invoke(self.$once, self, self.record, false);
                        self.$once = [];
                    }
                    return;
                }
                self.fireUpdates();
            } catch(e) {
                if(Date.now() - time < 10000) setTimeout(function() { self.write(fn, onComplete, defer, time); }, 100);
                console.error(e + " " + e.message + " " + e.stack);
                return;
            }

        }

        self.$writeFunctions.push(function() { self.write(fn, null, defer,time); });

        if(!self.writeTimer ) {
            self.writeTimer = setTimeout(write, 1000);

        }

        return self;
        function write() {
	        if(!self.isValid) {
		        console.warn("Unable to write " + self.type + ' ' + self.id);
		        return ;
	        }
            console.log("writing");
            self.writing = true;
            delete self.writeTimer;

	        process(self.record.data);

            $.ajax({
                url: serverAddress + self.id + "/" + self.type + "/" + self.uniqueId,
                type: 'post',
                async: true,
                dataType: 'json',
                data: {
                    type: self.type,
                    id: self.id,
                    revision: self.record.revision,
                    data: self.record.data
                }
            })
                .fail(function (a,b,c) {
		            console.error(c + " " + b + " from " + serverAddress + self.id + "/" + self.type);
                    self.writing = false;
                })
                .success(function (result) {
                    self.writing = false;
                    self.record = result.data;
                    self.updateStructure();
                    if (typeList[result.data.type]) {
                        Radical.invoke(typeList[result.data.type], self.id, self,
                            result.data.$notwritten$ ? false : true);
                    }
                    if (fn && result.data.$notwritten$) {
                        console.log("try again");
	                    self.fireUpdates();
	                    if (self.$once.length > 0) {
		                    Radical.invoke(self.$once, self, self.record, true);
		                    self.$once = [];
	                    }
	                    var writeFns = self.$writeFunctions;
	                    self.$writeFunctions = [];
	                    Radical.invoke(writeFns, self);
                        Radical.invoke(self.$deferred, self, self.record);
                    }
                    else {
	                    self.$writeFunctions = [];
                        Radical.invoke(self.$written, self, self.record);
                        if (self.$once.length > 0) {
                            Radical.invoke(self.$once, self, self.record, true);
                            self.$once = [];
                        }
                    }
                });

        };

    };

    function Id(spec) {
        this.id = spec.id;
        this.lastUpdateTime = spec.lastUpdateTime || new Date(0);
        this.data =  {};
        this.elements = {};
        this.once = [];
    }
    Id.prototype.type = function(type) {
        return this.data[type].data;
    };

    var updateList = [];
    var typeList = {};
	var skipped = 0;

    function checkForUpdates() {
        $.each(ids, function(i, id) {
	        id.skipped = (id.skipped || 0) + 1;
	        id.skipTests = id.skipTests || 0;
	        if(id.skipped < Math.sqrt(id.skipTests))
		        return;
	        id.skipped = 0;

	        $.ajax({
                url: serverAddress + "updated/" + id.id + "/" + id.lastUpdateTime,
                dataType: 'json',
                type: 'get'
            })
                .success(function (update) {

		            var oldUpdate = id.lastUpdateTime || 0;
                    id.lastUpdateTime = update.updateTime;
		            if(update.data && update.data.length == 0) {
			            id.skipTests = Math.min(maxSkipTests, (id.skipTests || 0) + 1);
		            }
		            else id.skipTests = 0;
		            $.each(update.data, function(i, data) {
			            var element = id.elements[data.type];
			            if(data.revision <= element.record.revision) return;

			            console.log("UPDATED " + data.type + " " + (new Date(data.lastUpdateTime)).toISOString() + " " + (new Date(oldUpdate)).toISOString());

			            id.data[data.type] = data;
			            if (element) {
				            element.record = data;
				            element.updateStructure();
				            Radical.invoke(element.$updated, data.data, element);
			            }
			            Radical.invoke(updateList, id, element);
			            if (typeList[data.type]) {
				            Radical.invoke(typeList[data.type], id, element, data.$notwritten$ ? false : true);
			            }
		            });
                });
        });
    }

    function addId(id) {
        ids[id] = new Id({id: id});
    }

    function removeId(id) {
        delete ids[id];
    }


    var that = {
        checkForUpdates: checkForUpdates,
        create: function(spec) {
            if(elements[getKey(spec)]) elements[getKey(spec)].fireUpdates();
            return elements[getKey(spec)] || new DataElement(spec);
        },
        onupdate: function(fn) {
            updateList.push(fn);
            return fn;
        },
        offupdate: function(fn) {
            var idx = updateList.indexOf(fn);
            if(idx !=-1) updateList.splice(idx,1);
            return fn;
        },
        ontypeupdate: function(type, fn) {
            typeList[type] = typeList[type] || [];
            typeList[type].push(fn);
            return fn;
        },
        offtypeupdate: function(type, fn) {
            var updateList = typeList[type] = typeList[type] || [];
            var idx = updateList.indexOf(fn);
            if(idx !=-1) updateList.splice(idx,1);
            return fn;
        },
        DataTypes: DataTypes

    };
	function verifyStructure(data, structure) {
		data = data || {};
		structure = structure || {};
		var defaultStructure = structure.$default;
		for(var i in structure) {
			(function(i) {
				if (structure.hasOwnProperty(i)) {
					var detail = structure[i] || defaultStructure;
					switch (detail.type || detail || 'default') {
						case 'int':
							data[i] = Math.floor(convertToNumber(data[i]));
							break;
						case 'float':
						case 'number':
							data[i] = convertToNumber(data[i]);
							break;
						case 'dictionary':
						case 'lookup':
						case 'dic':
						case 'collection':
							for(var j in data[i]) {
								verifyStructure(data[i][j], detail.structure);
							}
							break;
						case 'object':
							data[i] = data[i] || {};
							verifyStructure(data[i], detail.structure);
							break;
						case 'array':
							data[i] = data[i] || [];
							data[i].every(function (v) {
								verifyStructure(v, detail.structure);
								return true;
							});
							break;
						case 'bool':
						case 'boolean':
							data[i] = (data[i] == true || data[i] == "true");
							if(!data[i] || data[i] == "false") {
								//delete data[i];
							}
							break;
						default:
							data[i] = data[i] || "";
							break;
					}
				}
			})(i);
		}
	}

    Object.defineProperties(that, {
        timeOutDuration: {
            get: function() { return timeOutDuration; },
            set: function(v) {
                timeOutDuration = v;
                if(timerId) {
                    clearTimeout(timerId);

                }
	            setInterval(checkForUpdates, v);
            }
        }
    });

    that.timeOutDuration = timeOutDuration;

    return that;

});
