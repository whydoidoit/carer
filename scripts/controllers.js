var controllers = angular.module('CarersApp.controllers', []);

controllers.controller('exampleController', ['$scope', 'storedObject', function($scope, storedObject) {

	console.log("created controller");
	storedObject($scope, 'data', 'info', '__test__');

	//data.write(function() { if(!this.name) this.name = "Mike";});

}]);

var watcher = controllers.factory('storedObject', ['userData',  function(userData){

	function writeUpdated(userData, current, former) {
		function writeIfNecessary(items,real) {
			$.each(items, function(i,v) {
				var previous = former[i];
				if(v != previous) {
					userData.write(function() {
						if(real[i] != previous) return false;
						real[i] = v;
					});
				}
				else if(Radical.isArray(v)) {
					if(previous.length != v.length) {
						userData.write(function() {
							real[i] = v;
						})
					} else {
						writeIfNecessary(v, real[i]);
					}
				} 
				else if(Radical.isObject(v)) {
					writeIfNecessary(v, real[i]);
				}
				
			});
		}
		writeIfNecessary(current, userData.fields);

	}

	var dontWatchCount = 1;

	return function userVariable($scope, name, type, id){
		var data = userData.create({type: type, id: id});
		//This contains the last data we had
		var lastWritten = JSON.parse(JSON.stringify(data.fields));
		var current = JSON.parse(JSON.stringify(data.fields));
		$scope[name] = current;
		$scope.$watch(name, function(){
			//This is called when the data has been changed
			if(dontWatchCount>0) {
				dontWatchCount--;
				return;
			}
			writeUpdated(data, current, lastWritten);
			lastWritten = JSON.parse(JSON.stringify(current));
		}, true);
		data.updated(function(){
			dontWatchCount++;
			lastWritten = JSON.parse(JSON.stringify(this));
			current = JSON.parse(JSON.stringify(this));
			$scope[name] = current;
			$scope.$apply();
		});
	};
}])