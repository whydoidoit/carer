
var Radical = Radical || {};
var baseAddress = "http://aws1.radicalrealms.com:3000";
//var baseAddress = "http://localhost:3000";

function convertToNumber(something) {
	var points = 0;
	try {
		points = parseInt(something);
	} catch(e) {
	}
	if(isNaN(points)) points = 0;
	return points;
}


Radical.oldConsole = Radical.oldConsole || window.console;
window.onerror = window.onerror; /*function(errmsg, url, linenumber) {
	window.console.error("URL: " + url + " LINE: " + linenumber + " MSG: " + errmsg);
	window.console.trace();
	return true;
};*/
var alreadyLogging;
window.console = window.console || {
	log: function(toLog, tag) {

		toLog = (new Date()).toISOString() + ": " + toLog;
		tag = tag || "RADICAL ";
		if(alreadyLogging) return;
		alreadyLogging = true;
		Radical.oldConsole.log(tag + toLog);
		alreadyLogging = false;
		$.ajax({
			url: baseAddress + "/log/" + encodeURIComponent("INFO: " + toLog + '\n')
		});
	},
	warn: function(toLog, tag) {
		toLog = (new Date()).toISOString() + ": " + toLog;
		tag = tag || "RADICAL ";
		if(alreadyLogging) return;
		alreadyLogging = true;
		Radical.oldConsole.warn(tag + toLog);
		alreadyLogging = false;
		$.ajax({
			url: baseAddress + "/log/" + encodeURIComponent("WARN: " + toLog + '\n')
		});
	},
	error: function(toLog, tag) {
		tag = tag || "RADICAL ";
		toLog = (new Date()).toISOString() + ": " + toLog;
		if(alreadyLogging) return;
		alreadyLogging = true;
		Radical.oldConsole.error(tag + toLog);
		alreadyLogging = false;
		$.ajax({
			url: baseAddress + "/log/" + encodeURIComponent("ERR : " + toLog + '\n')
		});
	},

	trace: function() { oldConsole.trace(); }
}

$.fn.sequenceEqual = function(compareTo) {
    if (!compareTo || !compareTo.length || this.length !== compareTo.length) {
        return false;
    }
    for (var i = 0, length = this.length; i < length; i++) {
        if (this[i] !== compareTo[i]) {
            return false;
        }
    }
    return true;
}

function replaceAll(find, replace, str) {
    if(!str)
        return str;
    return str.replace(new RegExp(find, 'g'), replace);
}

Radical.debugging = true;
Radical.handleExceptions = true;

Radical.invoke = function (array, refThis) {

    if(array.$processing) {
        return;
    }

    array.$processing = true;

    var i;
    var prms = [];
    refThis = refThis || this;
    for (i = 2; i < arguments.length; i++) {
        prms.push(arguments[i]);
    }

    for (i in array) {
        if(!array.hasOwnProperty(i)) continue;
        if(!array[i].apply) continue;

        if(!Radical.handleExceptions) {
            array[i].apply(refThis, prms);
        } else {
            try {
                array[i].apply(refThis, prms);
            } catch(e) {
                if(Radical.debugging) {
                    console.error(e);
                    console.error(e.stack);
                }
            }
        }
    }
    array.$processing = false;
};
window.logging = function(level) {
	var console = window.console;
	level = level || 'normal';
	switch(level) {
		case 'normal':
			return console;
		case 'warn':
			return {
				log: function() {},
				warn: function(toLog, tag) {
					console.warn(toLog, tag);
				},
				error: function(toLog, tag) {
					console.error(toLog, tag);
				}
			};
		case 'error':
			return {
				log: function() {},
				warn: function() {},
				error: function(toLog, tag) {
					console.error(toLog, tag);
				}
			};
	}
}


function makeNameArray(obj) {
	var result = [];
	if(obj) {
		$.each(obj, function (i, v) {
			if(i.substring(0,2)=="__") return;
			result.push(v);
		});
	}
	return result;
}


function makeArray(obj) {
	var result = [];
	if(obj) {
		$.each(obj, function (i, v) {
			if(i.substring(0,2)=="__") return;
			result.push(i);
		});
	}
	return result;
}

function convertDate(date, getting) {
    getting = getting || true;
    if(getting) {
        return (new Date(Date.parse(date))).toDateInputValue();
    } else {
        return new Date(Date.parse(date));
    }
}

String.format = function() {
    var s = arguments[0];
    for (var i = 0; i < arguments.length - 1; i++) {
        var reg = new RegExp("\\{" + i + "\\}", "gm");
        s = s.replace(reg, arguments[i + 1]);
    }

    return s;
}

Radical.isObject = function (test) {
    return (typeof test == "object") && test != null;
};


Radical.isArray = function(array) {
    return $.isArray(array);
    //return array instanceof Array || (array && array.$isArray) || Object.prototype.toString.call( array ) === '[object Array]';
};

Radical.newGuid = function() {
    return 'xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
        return v.toString(16).toUpperCase();
    });
}

