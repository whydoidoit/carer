(function (global) {
    var app = global.appKendo = global.appKendo || {};

    document.addEventListener('deviceready', function () {
        navigator.splashscreen.hide();

        // Initialize Everlive SDK
        var el = new Everlive({
            apiKey: appSettings.everlive.apiKey,
            scheme: appSettings.everlive.scheme
        });

        //Initialize the current app and layout
        app.application = new kendo.mobile.Application(document.body, { layout: "tabstrip-layout" });
    }, false);
})(window);